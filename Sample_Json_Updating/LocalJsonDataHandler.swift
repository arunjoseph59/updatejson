//
//  LocalJsonDataHandler.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import Foundation

enum URLType {
    case mainURl
    case subUrl
}

class LocalJsonDataHandler {
    
    typealias LoadedFile = (subPath: URL?, mainPath: URL?)
    
    static let shared = LocalJsonDataHandler()
    private let fileManager = FileManager.default
    var subUrl: URL?
    
    func getJsonFileUrl(with fileName: String)-> LoadedFile? {
        guard let mainUrl = Bundle.main.url(forResource: fileName , withExtension: "json") else {return nil}
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileNameWithExtension = fileName+".json"
            subUrl = documentDirectory.appendingPathComponent(fileNameWithExtension)
            print(loadFile(subUrl: subUrl!, mainUrl: mainUrl))
            return loadFile(subUrl: subUrl!, mainUrl: mainUrl)
        } catch {
            return nil
        }
    }
    
   
    private func loadFile(subUrl: URL, mainUrl: URL)-> LoadedFile {
        if fileManager.fileExists(atPath: subUrl.path){
            return LoadedFile(subPath: subUrl , mainPath: mainUrl)
        }
        else{
            return LoadedFile(subPath: nil , mainPath: mainUrl)
        }
    }
    
    
    func decodeData<T: Codable>(pathName: URL, responseType: T.Type)-> T? {
        do{
            let jsonData = try Data(contentsOf: pathName)
            let decoder = JSONDecoder()
            let decodedData = try decoder.decode(responseType, from: jsonData)
            return decodedData
        } catch {
            return nil
        }
    }
    
    func wirteToJsonFile<T: Encodable>(location: URL, data: T) {
        do{
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let JsonData = try encoder.encode(data)
            try JsonData.write(to: location)
        }catch{
            print(error.localizedDescription)
        }
    }
}

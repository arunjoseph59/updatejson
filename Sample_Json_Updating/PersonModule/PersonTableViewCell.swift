//
//  PersonTableViewCell.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var nationality: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

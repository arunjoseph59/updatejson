//
//  EditPersonViewModel.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import Foundation


class EditPersonViewModel {
    
    var personList: [Person]?
    var currentIndex: Int?
    
    let localJsonHandler = LocalJsonDataHandler.shared
    
    init(personList: [Person], index: Int) {
        self.personList = personList
        self.currentIndex = index
    }
    
    func updateToJsonFile(data: [Person]) {
        guard let subUrl = localJsonHandler.subUrl else{return}
        localJsonHandler.wirteToJsonFile(location: subUrl, data: data)
    }
}

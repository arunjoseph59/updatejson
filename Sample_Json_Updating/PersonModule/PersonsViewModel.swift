//
//  PersonsViewModel.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import Foundation

protocol PersonViewModelDelegate: AnyObject {
    func updatePersonList()
}

class PersonsViewModel {
    
    let loacalJsonHandler = LocalJsonDataHandler.shared
    var personList: [Person] = []
    weak var delegate: PersonViewModelDelegate?
    
    //MARK:- Fetch
    func fetch() {
        let paths = loacalJsonHandler.getJsonFileUrl(with: "Person")
        if personList.isEmpty {
            guard let path = paths?.mainPath, let person = loacalJsonHandler.decodeData(pathName: path, responseType: [Person].self) else {return}
            self.personList = person
            delegate?.updatePersonList()
        }
        else{
            guard let path = paths?.subPath, let person = loacalJsonHandler.decodeData(pathName: path, responseType: [Person].self) else{return}
            self.personList = person
            delegate?.updatePersonList()
        }
    }
}

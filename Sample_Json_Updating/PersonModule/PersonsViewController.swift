//
//  PersonsViewController.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import UIKit

class PersonsViewController: UIViewController {

    @IBOutlet weak var personsTableView: UITableView!
    private let viewModel = PersonsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.delegate = self
        viewModel.fetch()
    }
}

extension PersonsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.personList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PersonTableViewCell
        cell.name.text = viewModel.personList[indexPath.row].name
        cell.age.text = viewModel.personList[indexPath.row].age
        cell.nationality.text = viewModel.personList[indexPath.row].nationality
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let editPersonVc = EditPersonViewController.instantiate()
        editPersonVc.viewModel = EditPersonViewModel(personList: viewModel.personList, index: indexPath.row)
        self.navigationController?.pushViewController(editPersonVc, animated: true)
    }
}

extension PersonsViewController: PersonViewModelDelegate {
    func updatePersonList() {
        personsTableView.reloadData()
    }
}

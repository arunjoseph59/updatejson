//
//  Person.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import Foundation

struct Person: Codable {
    var id: Int
    var name: String
    var age: String
    var nationality: String
}

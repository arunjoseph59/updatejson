//
//  EditPersonViewController.swift
//  Sample_Json_Updating
//
//  Created by Arun Joseph on 07/09/21.
//

import UIKit

class EditPersonViewController: UIViewController, Storyboarded {
    static var storyboardName: String {"Main"}
    
    @IBOutlet weak var nameTextFld: UITextField!
    @IBOutlet weak var nationalityTextFld: UITextField!
    @IBOutlet weak var ageTextFld: UITextField!
    
    var viewModel: EditPersonViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let persons = viewModel?.personList, let index = viewModel?.currentIndex else{return}
        setData(of: persons[index])
    }
    
    private func setData(of person: Person) {
        self.nameTextFld.text = person.name
        self.ageTextFld.text = person.age
        self.nationalityTextFld.text = person.nationality
    }
    
    @IBAction func updateButtonAction(_ sender: Any) {
        guard var persons = viewModel?.personList, let index = viewModel?.currentIndex else {return}
        guard let name = nameTextFld.text, let age = ageTextFld.text, let nationality = nationalityTextFld.text else{return}
        persons[index].age = age
        persons[index].name = name
        persons[index].nationality = nationality
        viewModel?.updateToJsonFile(data: persons)
    }
}
